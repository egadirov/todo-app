## Installation

Clone the repo locally:

```sh
git clone https://egadirov@bitbucket.org/egadirov/todo-app.git todo-interview-app
cd todo-interview-app
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm ci
```

Build assets:

```sh
npm run dev
```

Setup config:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

Create an SQLite database.

```sh
touch database/database.sqlite
```

Run database migrations:

```sh
php artisan migrate
```

Run database seeder:

```sh
php artisan db:seed
```

Run the dev server:

```sh
php artisan serve
```
