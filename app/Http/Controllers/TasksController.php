<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class TasksController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $tasks = Task::orderByDesc('created_at')
            ->filter(request()->only('status'))
            ->with('assignee')
            ->paginate(10);

        if (request()->wantsJson()) {
            return response()->json($tasks);
        }

        return view('task.index', [
            'statuses' => Task::make()->getEnumValues('status'),
        ]);
    }

    public function show(Task $task)
    {
        return view('task.show', [
            'task' =>$task->load('assignee')
        ]);
    }

    public function create()
    {
        $task = new Task();

        return view('task.create', [
            'statuses' => $task->getEnumValues('status'),
            'users' => User::all()
        ]);
    }

    public function store(Request $request)
    {
        request()->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'status' => ['required', Rule::in([Task::STATUS_NOT_STARTED, Task::STATUS_IN_PROGRESS, Task::STATUS_COMPLETED])],
            'due_date' => ['nullable', 'date'],
            'assignee_id' => ['nullable', Rule::exists('users', 'id')],
        ]);

        Auth::user()->tasks()->create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'due_date' => $request->get('due_date'),
            'status' => $request->get('status'),
            'assignee_id' => $request->get('assignee_id'),
        ]);

        return redirect('/')->with('flash', 'Task has been created successfully!');
    }

    public function edit(Task $task)
    {

        return view('task.edit',[
            'task' => $task,
            'statuses' => $task->getEnumValues('status'),
            'users' => User::all()
        ]);
    }

    public function update(Request $request, Task $task)
    {
        $this->authorize('update-task', $task);

        request()->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'status' => ['required', Rule::in([Task::STATUS_NOT_STARTED, Task::STATUS_IN_PROGRESS, Task::STATUS_COMPLETED])],
            'due_date' => ['nullable', 'date'],
            'assignee_id' => ['nullable', Rule::exists('users', 'id')],
        ]);

        $task->update($request->only('title', 'description', 'status', 'due_date', 'assignee_id'));

        return redirect(route('index'))->with('flash', 'Task has been updated successfully');
    }

    public function delete(Task $task)
    {
        $this->authorize('delete-task', $task);

        $task->delete();

        if (request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/')->with('flash', 'Task has been deleted successfully');
    }

    public function changeTaskStatus(Task $task)
    {
        $task->update([
            'status' => $task->status === 'completed' ? 'in_progress' : 'completed'
        ]);

        return redirect()->back();
    }

}
