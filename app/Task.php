<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use EnumValues;

    protected $guarded = [];

    protected $appends = ['FormattedDueDate'];

    protected $casts = [
        'due_date' => 'datetime',
    ];

    const STATUS_NOT_STARTED = 'not_started';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_COMPLETED   = 'completed';



    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee_id');
    }

    public function getFormattedDueDateAttribute()
    {
        if(empty($this->due_date)) return '-';

        switch ($this->due_date){
            case $this->due_date->isToday():
                return "Today";
                break;
            case $this->due_date->isTomorrow():
                return "Tomorrow";
                break;
            case $this->due_date->isYesterday():
                return "Yesterday";
                break;
            default:
                return $this->due_date->diffForHumans();
        }

    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['status'] ?? null, function ($query, $search) {
            $query->where('status', 'like', '%'.$search.'%');
        });
    }

}
