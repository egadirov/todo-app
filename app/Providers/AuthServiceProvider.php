<?php

namespace App\Providers;

use App\Task;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('edit-assignee-task', function ($user, Task $task) {
            return $user->id === $task->author->id;
        });

        Gate::define('edit-task', function ($user, Task $task) {
            return $user->id === $task->author->id || $user->id === $task->assignee->id;
        });

        Gate::define('update-task', function ($user, Task $task) {
            return $user->id === $task->author->id || $user->id === $task->assignee->id;
        });

        Gate::define('delete-task', function ($user, Task $task) {
            return $user->id === $task->author->id;
        });
    }
}
