<?php


namespace App;


trait EnumValues
{
    public function getEnumValues($fieldName)
    {
        $reflectedClass = new \ReflectionClass($this);

        $possibleValues = array_filter($reflectedClass->getConstants(), function ($k) use ($fieldName) {
            return strpos($k, strtoupper($fieldName) . '_') === 0;
        }, ARRAY_FILTER_USE_KEY);
        $result = [];
        foreach ($possibleValues as $v) {
            $result[$v] = $this->enum_trans($this, $fieldName, $v);
        }
        return $result;
    }

    public function enum_trans(\Illuminate\Database\Eloquent\Model $model, $field, $value = null)
    {
        return trans('enum.' . strtolower(class_basename($model)) . '.' . $field . '.' . ($value ? $value : $model->$field));
    }

}
