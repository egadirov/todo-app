<div>
    <div class="bg-white shadow-md rounded my-6">
        <table class="text-left w-full border-collapse">
            <thead>
            <tr>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Name</th>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Description</th>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Status</th>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Due date</th>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Assignee</th>
                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
            <tr class="hover:bg-grey-lighter">
                <td class="py-4 px-6 border-b border-grey-light">{{ $task->title }}</td>
                <td class="py-4 px-6 border-b border-grey-light">{{ Str::limit($task->description, 50, $end = '...') }}</td>
                <td class="py-4  border-b border-grey-light">
                    <span class="rounded py-1 px-3 text-xs font-bold w-64 {{ 'status-' .str_replace("_", "-",  $task->status)  }}">{{ trans('enum.task.status.' . $task->status ) }}</span>
                </td>

{{--                <td class="py-4 px-6 border-b border-grey-light">{{ $task->due_date ? $task->due_date->diffForHumans() : '-'}}</td>--}}
                <td class="py-4 px-6 border-b border-grey-light">{{ $task->formated_due_date }}</td>
                <td class="py-4 px-6 border-b border-grey-light">{{ $task->assignee->name }}</td>
                <td class="py-4 px-6 border-b border-grey-light w-64 text-center">
                    <a href="{{ route('task.show', $task->id) }}" class="text-white font-bold py-1 px-3 rounded text-xs bg-blue-500 hover:bg-blue-700">View</a>
                    @can('edit-task', $task)
                        <a href="{{ route('task.edit', $task->id) }}" class="text-white font-bold py-1 px-3 rounded text-xs bg-green-500 hover:bg-green-700">Edit</a>
                    @endcan
                    @can('delete-task', $task)
                        <a href="{{ route('task.delete', $task->id) }}" data-toggle="modal" data-url="{{ route('task.delete', $task->id) }}" data-target="#exampleModal" data-task-id="{{ $task->id }}" class="modal-open text-white font-bold py-1 cursor-pointer px-3 rounded text-xs bg-red-500 hover:bg-red-700">Delete</a>
                    @endcan
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="flex justify-center mx-auto py-2">
    {{ $tasks->links() }}
</div>

