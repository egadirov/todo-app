@extends('layouts.app')

@section('content')
    <task-list :statuses="{{ json_encode($statuses) }}"></task-list>
@endsection
