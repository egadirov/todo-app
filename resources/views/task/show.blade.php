@extends('layouts.app')

@section('content')
    <div class="leading-loose ">
        <div class="max-w-xl rounded shadow-xl mx-auto bg-white">

            <div class="border-b border-gray-300 px-10 py-3 flex justify-between">
                @if($task->status !== 'completed')
                    <a href="{{ route('task.change_status', $task) }}"
                       class="bg-gray-300 text-gray-800  border border-gray-200 hover:border-green-700 font-bold cursor-pointer px-4 rounded inline-flex items-center hover:bg-green-300 hover:text-green-700">
                        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path class="heroicon-ui"
                                  d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-2.3-8.7l1.3 1.29 3.3-3.3a1 1 0 0 1 1.4 1.42l-4 4a1 1 0 0 1-1.4 0l-2-2a1 1 0 0 1 1.4-1.42z"/>
                        </svg>
                        <span>Mark as complete</span>
                    </a>
                @else
                    <a href="{{ route('task.change_status', $task) }}"
                       class="border-green-700 font-bold cursor-pointer px-4 rounded inline-flex items-center bg-green-300 text-green-700">
                        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path class="heroicon-ui"
                                  d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-2.3-8.7l1.3 1.29 3.3-3.3a1 1 0 0 1 1.4 1.42l-4 4a1 1 0 0 1-1.4 0l-2-2a1 1 0 0 1 1.4-1.42z"/>
                        </svg>
                        <span>Completed</span>
                    </a>
                @endif
                <span class="pr-2 text-sm text-gray-600">Created by:
                    <a href="" class="">{{ $task->author->name }}</a>
                </span>
            </div>

            <div class="p-10">
                <div>
                    <h3> {{ $task->title }} </h3>
                </div>
                <div class="mt-3 flex">
                    <div class="w-32">
                        <label class="block text-sm text-gray-700" for="assignee">Assignee</label>
                    </div>
                    <div class="w-full">
                        <p class="text-gray-800 font-medium">{{ $task->assignee->name }}</p>
                    </div>
                </div>
                <div class="mt-2 flex">
                    <div class="w-32">
                        <label class="block text-sm text-gray-700" for="due_date">Due date</label>
                    </div>
                    <div class="w-full">
                        <p class="text-gray-800 font-medium">{{ $task->formated_due_date }}</p>
                    </div>
                </div>
                <div class="mt-2 flex">
                    <div class="w-32">
                        <label class="block text-sm text-gray-700" for="description">Description</label>
                    </div>
                    <div class="w-full">
                        <p class="text-gray-800 font-medium">{{ $task->description }}</p>
                    </div>
                </div>
                <div class="mt-2 flex">
                    <div class="w-32">
                        <label class="block text-sm text-gray-700" for="status">Status</label>
                    </div>
                    <div class="w-full">
                        <span class="rounded py-1 px-3 text-xs font-bold w-64 {{ 'status-' .str_replace("_", "-",  $task->status)  }}">{{ trans('enum.task.status.' . $task->status ) }}</span>
                    </div>
                </div>

            </div>
            @can('edit-task', $task)
                <div class="border-t border-gray-300 px-10 py-3 flex justify-end">
                    <a href="{{ route('task.edit', $task->id) }}" class="text-gray-200 font-bold py-1 px-3 rounded bg-green-500 hover:text-white hover:bg-green-700">Edit</a>
                </div>
            @endcan
        </div>
    </div>
@endsection


