@extends('layouts.app')

@section('content')
    <div class="leading-loose ">
        <form class="max-w-xl m-4 p-10 bg-white rounded shadow-xl mx-auto" action="{{ route('task.update', $task->id) }}" method="POST">
            @csrf
            @method('put')
            <p class="text-gray-800 font-medium">Edit task</p>
            <div class="">
                <label class="block text-sm text-gray-700" for="title">Title</label>
                <input value="{{ $task->title }}" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded {{ $errors->has('title') ? 'border-2 border-red-500' :'' }}" id="title" name="title"
                       type="text" placeholder="Task title" aria-label="Name">
                @if($errors->has('title'))
                    <p class="text-red-500 text-xs italic">{{ $errors->first('title') }}</p>
                @endif
            </div>
            <div class="mt-2">
                <label class="block text-sm text-gray-700" for="description">Description</label>
                <textarea class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded {{ $errors->has('description') ? 'border-2 border-red-500' :'' }}" id="description" name="description" type="text"
                          placeholder="Task description" aria-label="Email">{{ $task->description }}"
                </textarea>
                @if($errors->has('description'))
                    <p class="text-red-500 text-xs italic">{{ $errors->first('description') }}</p>
                @endif
            </div>
            <label class="block text-sm text-gray-700 mt-2" for="status">Due Date</label>
            <input type="date" name="due_date" id="due_date" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded {{ $errors->has('description') ? 'border-2 border-red-500' :'' }}">
            @if($errors->has('due_date'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('due_date') }}</p>
            @endif
            <label class="block text-sm text-gray-700 mt-2" for="status">Status</label>
            <div class="relative ">
                <select class="block appearance-none w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="status" name="status">
                    @foreach($statuses as $key => $status)
                        <option value="{{ $key }}" {{ $task->status == $key ? 'selected' : '' }}>
                            {{ $status }}
                        </option>
                    @endforeach
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                </div>
            </div>
            @can('edit-assignee-task', $task)
                <label class="block text-sm text-gray-700 mt-2" for="assignee">Assign this task</label>
                <div class="relative ">
                    <select class="block appearance-none w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="assignee" name="assignee_id">
                        <option value="{{ Auth::user()->id }}">{{ Auth::user()->name}}</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                        </svg>
                    </div>
                </div>
            @endcan
            <div class="mt-4 flex justify-end">
                <button class="text-white font-bold py-1 px-3 rounded text-xs bg-green-500 hover:bg-green-700" type="submit">Update</button>
            </div>
        </form>
    </div>
@endsection


