<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                Are you sure ?
                <input type="text" id="task_id" name="task_id" value=""/>
            </div>
            <form id="taskForm" class="modal-footer" method="POST" action="">
                @csrf
                @method('DELETE')
                <a data-dismiss="modal" class="modal-open font-bold py-1 cursor-pointer px-3 rounded text-xs border hover:bg-gray-500">Cancel</a>
                <button type="submit" class="modal-open text-white font-bold py-1 cursor-pointer px-3 rounded text-xs bg-red-500 hover:bg-red-700">Delete</button>
            </form>
        </div>
    </div>
</div>
