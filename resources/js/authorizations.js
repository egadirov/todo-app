let user = window.App.user;

module.exports = {
    owns (model, prop = 'user_id') {
        return model[prop] === user.id
    },
    assign (model, prop = 'assignee_id') {
        return model[prop] === user.id
    },
};
