export default {
    methods: {
        updateUrl(field, value) {
            const url = new URL(window.location.href)
            const search_params = new URLSearchParams(url.search)
            if (!value || value.length === 0) {
                search_params.delete(field)
            } else {
                search_params.set(field, value)
            }
            url.search = search_params.toString()

            history.pushState({}, null, url)

        },
        urlQueries() {
            return (location.search.match(new RegExp("([^?=&]+)(=([^&]*))?", 'g')) || [])
                .reduce(function (result, each, n, every) {
                    let [key, value] = each.split('=')
                    result[key] = value
                    return (result)
                }, {})

        },
    }
}
