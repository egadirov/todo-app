<?php

return [

    'task' => [
        'status' => [
            'not_started' => 'Not started',
            'in_progress' => 'In progress',
            'completed' => 'Completed'
        ]
    ],

];
