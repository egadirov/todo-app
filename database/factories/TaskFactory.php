<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Task;
use App\User;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
//    $users = factory(User::class, 10)->create();

    return [
        'title' => $faker->jobTitle,
        'description' => $faker->realText(50),
        'status' => array_rand(Task::make()->getEnumValues('status')),
//        'due_date' => $faker->date(),
        'due_date' => $faker->dateTimeBetween($startDate = '-1 day', $endDate = '+1 day', $timezone = null),
        'user_id' => factory(User::class)->create(),
        'assignee_id' => factory(User::class)->create()
    ];
});
