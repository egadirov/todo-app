<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'TasksController@index')->name('index');
Route::get('tasks/create', 'TasksController@create')->name('task.create');
Route::get('tasks/{task}', 'TasksController@show')->name('task.show');
Route::post('tasks', 'TasksController@store')->name('task.store');
Route::get('tasks/{task}/edit', 'TasksController@edit')->name('task.edit');
Route::put('tasks/{task}', 'TasksController@update')->name('task.update');
Route::delete('tasks/{task}', 'TasksController@delete')->name('task.delete');

Route::get('tasks/status/{task}', 'TasksController@changeTaskStatus')->name('task.change_status');

