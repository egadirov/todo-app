const mix = require('laravel-mix');
const cssImport = require('postcss-import')
const cssNesting = require('postcss-nesting')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css/auth.css')
   .postCss('resources/css/app.css', 'public/css', [
       require('tailwindcss'),
   ])
    .options({
    postCss: [
        cssImport(),
        cssNesting(),
        ...mix.inProduction() ? [
            purgecss({
                content: ['./resources/views/**/*.blade.php', './resources/js/**/*.vue'],
                defaultExtractor: content => content.match(/[\w-/:.]+(?<!:)/g) || [],
            }),
        ] : [],
    ],
})

